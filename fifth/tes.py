#solution 5.2
testDict = {
	"Data A" : {"Sub Data A":"valueA"},
	"Data B" : {"Sub Data B":"valueB"},
	"Data C" : {"Sub Data C":"valueC"},
	"Data D" : {"Sub Data D":"valueD"},

}

for key in testDict:
    t = []
    for key2 in testDict[key]:
        t.append([key,key2, testDict[key][key2]])
        print(t)
        
#one line solution using list comprehension
lix = {k1: [[k2, v2] for k2, v2 in v1.items()] 
          for k1, v1 in testDict.items()}

ans = [[k, *v[0]] for k,v in lix.items()]
print("\n solution using list comprehension",ans)