# solution first problem 5.1
testDict = {
	"Data A" : {"Sub Data A":"valueA"},
	"Data B" : {"Sub Data B":"valueB"},
	"Data C" : {"Sub Data C":"valueC"},
	"Data D" : {"Sub Data D":"valueD"},

}
newdict = {}
for key in testDict:
    t = []
    for key2 in testDict[key]:
        t.append([key2, testDict[key][key2]])
        print(t)
    newdict[key] = t
print(newdict)

# using list comprehension the solution or one line solution
q1 = {k1: [[k2, v2] for k2, v2 in v1.items()] 
          for k1, v1 in testDict.items()}
print(q1)