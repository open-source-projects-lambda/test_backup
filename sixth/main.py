var = [["Data A","Sub Data A","valueA"],
	["Data B","Sub Data B","valueB"],
	["Data C","Sub Data C","valueC"],
	["Data D","Sub Data D","valueD"]]
result = {k: {v[0]: v[1]} for k, *v in var}
print(result)